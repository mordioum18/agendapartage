import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CalendrierPage} from '../calendrier/calendrier.page';
import {FormsModule} from '@angular/forms';
import {FullCalendar, FullCalendarModule} from 'primeng/fullcalendar';

@NgModule({
  declarations: [CalendrierPage],
  imports: [
    CommonModule,
      FullCalendarModule
  ],
    exports: [
        CommonModule,
        FormsModule,
        CalendrierPage
    ]
})
export class SharedModuleModule { }
