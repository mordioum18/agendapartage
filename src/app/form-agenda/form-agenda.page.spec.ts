import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAgendaPage } from './form-agenda.page';

describe('FormAgendaPage', () => {
  let component: FormAgendaPage;
  let fixture: ComponentFixture<FormAgendaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAgendaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAgendaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
