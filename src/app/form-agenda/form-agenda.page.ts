import { Component, OnInit } from '@angular/core';
import {Agenda} from '../domaine/agenda';
import {AgendaService} from '../service/agenda.service';
import {UserService} from '../service/user.service';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-form-agenda',
  templateUrl: './form-agenda.page.html',
  styleUrls: ['./form-agenda.page.scss'],
})
export class FormAgendaPage implements OnInit {

  agenda: Agenda ;
  constructor(private agendaService: AgendaService, private userServie: UserService, private modalController: ModalController, ) {
    this.agenda = new Agenda();

}
  ngOnInit(){
  }
  create() {
    this.agendaService.create(this.agenda, [1] ).subscribe(
        data => {
          console.log(data) ;
          this.agenda = data ;
          const data3 = this.agenda ;
          this.userServie.createUserAgenda(1, data.id).subscribe(
            data2 => {
              console.log(data2);
                this.modalController.dismiss({data3}) ;
            }
          )
        }
    )

  }
}
