import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FormAgendaPage } from './form-agenda.page';
import {AgendaService} from '../service/agenda.service';
import {UserService} from '../service/user.service';

const routes: Routes = [
  {
    path: '',
    component: FormAgendaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FormAgendaPage],
    providers: [AgendaService, UserService]
})
export class FormAgendaPageModule {}
