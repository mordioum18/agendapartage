import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {IonSlides, NavController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {CalendrierPage} from '../calendrier/calendrier.page';

@Component({
  selector: 'app-calendrier-jour',
  templateUrl: './calendrier-jour.page.html',
  styleUrls: ['./calendrier-jour.page.scss'],
})
export class CalendrierJourPage implements OnInit {
    @ViewChild('slider') private slider: IonSlides;
    @ViewChild('currentDay') private currentDay: CalendrierPage;
    @ViewChild('nextDay') private nextDay: CalendrierPage;
    @ViewChild('prevDay') private prevDay: CalendrierPage;
    defaultDay: Date;
    currentDate: Date;
    lastMaxDate: Date;
    lastMinDate: Date;
    options1: any;
    options2: any;
    options3: any;

    events: any[];
    firstSlide = true;
    slideOpts = {
        spaceBetween: 10,
    };


    constructor(private router: ActivatedRoute, private navCtrl: NavController) {
    }

    ngOnInit() {
        const tmpParam = this.router.snapshot.queryParamMap.get('date') ;
        if (tmpParam !== null && tmpParam !== undefined){
            this.defaultDay = new Date(tmpParam) ;
        }
        else {
            this.defaultDay = new Date();
        }
        this.events = [
            {
                'title': 'All Day Event',
                'start': '2019-01-01',
            },
            {
                'title': 'Long Event',
                'start': '2019-01-07',
                'end': '2019-01-10'
            },
            {
                'title': 'mor dioum',
                'start': '2019-01-07',
                'end': '2019-01-10',
                'color': '#378006'
            },
            {
                'title': 'Repeating Event',
                'start': '2019-01-09T16:00:00'
            },
            {
                'title': 'Repeating Event',
                'start': '2019-01-16T16:00:00',
                'color': '#378006'
            },
            {
                'title': 'Conference',
                'start': '2019-01-11',
                'end': '2019-01-13'
            }
        ];
        this.currentDate = this.defaultDay ;
        this.lastMinDate = this.minusJour(this.currentDate);
        this.lastMaxDate = this.addJour(this.currentDate);
        this.createOption();


    }
    createOption(){
        this.options2 = {
            defaultDate: this.defaultDay,
            defaultView: 'agendaDay',
            height: 'parent',
            locale: 'fr',
            buttonText: {
                month: 'Mois',
                week: 'Semaine',
                day: 'Jour',
                list:  'List'
            },
            header: {
                left: '',
                right: ''
            },
            dateClick: (e) => {
                console.log(e.dateStr) ;
            }
        };
        this.options1 = JSON.parse(JSON.stringify(this.options2));
        this.options1.defaultDate = this.lastMinDate;
        this.options3 = JSON.parse(JSON.stringify(this.options2));
        this.options3.defaultDate = this.lastMaxDate;
    }
    onSlideNextEnd(){
        this.currentDay.fc.getCalendar().next();
        this.nextDay.fc.getCalendar().next();
        this.prevDay.fc.getCalendar().next();
        this.currentDate = new Date(this.currentDay.fc.getCalendar().getDate());
        this.slider.slideTo(1, 0, false) ;
    }
    onSlidePrevEnd(){
        this.currentDay.fc.getCalendar().prev();
        this.nextDay.fc.getCalendar().prev();
        this.prevDay.fc.getCalendar().prev();
        this.currentDate = new Date(this.currentDay.fc.getCalendar().getDate());
        this.slider.slideTo(1, 0, false) ;
    }
    onChange(event){
    }
    addJour(date: Date){
        const day3 = new Date(date);
        day3.setDate(date.getDate() + 1);
        return day3;
    }
    minusJour(date: Date) {
        const day1 = new Date(date);
        day1.setDate(date.getDate() - 1);
        return day1;
    }
    ngAfterViewInit() {
        this.slider.slideTo(1, 0, false);
    }
    navigateToAdd_evenement() {
        this.navCtrl.navigateForward('/add-evenement')
    }

}
