import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendrierJourPage } from './calendrier-jour.page';

describe('CalendrierJourPage', () => {
  let component: CalendrierJourPage;
  let fixture: ComponentFixture<CalendrierJourPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendrierJourPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendrierJourPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
