import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CalendrierJourPage } from './calendrier-jour.page';
import {SharedModuleModule} from '../shared-module/shared-module.module';
import {FullCalendarModule} from 'primeng/fullcalendar';
import {HttpClientModule} from '@angular/common/http';

const routes: Routes = [
  {
    path: '',
      children: [
          { path: '', component: CalendrierJourPage },
          { path: 'gotodate', component: CalendrierJourPage },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
      FullCalendarModule,
      SharedModuleModule,
  ],
  declarations: [CalendrierJourPage],
    entryComponents: [CalendrierJourPage]
})
export class CalendrierJourPageModule {}
