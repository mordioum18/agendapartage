import {UserEvenementPk} from './userEvenementPk';

export class UserEvenement {
    public dateDebut: number ;
    public dateFin: number;
    public pk: UserEvenementPk ;
}