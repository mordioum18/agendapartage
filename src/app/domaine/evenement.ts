import {UserEvenement} from './userEvenement';
import {Calendrier} from './calendrier';

export class Evenement{
    public nom: string ;
    public dateCreation: number;
    public dateDebut: number;
    public dateFin: number;
    public description: string ;
    public estFige: boolean;
    public userEvenements: UserEvenement[];
    public calendrier: Calendrier;
    public notifications;
}