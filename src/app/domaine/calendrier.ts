import {Evenement} from './evenement';
import {Agenda} from './agenda';

export class Calendrier  {
    public id: number;
    public nom: String;
    public dateCreation: number;
    public agenda: Agenda;
    public evenements: Evenement[] ;
}