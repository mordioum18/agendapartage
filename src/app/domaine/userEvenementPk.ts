import {Evenement} from './evenement';
import {User} from './user';

export class UserEvenementPk {
    user: User ;
    evenement: Evenement ;
}