import {UserEvenement} from './userEvenement';

export class User {
    public id: number ;
    public nom: string ;
    public prenom: string ;
    public email: string ;
    public telephone: string;
    public userEvenements: UserEvenement ;

}