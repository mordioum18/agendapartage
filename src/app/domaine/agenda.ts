import {UserEvenement} from './userEvenement';
import {UserAgenda} from './userAgenda';

export class  Agenda {
    public  id;
    public nom;
    public calendriers;
    public userAgenda: UserAgenda;
    public  dateCreation: number ;
}