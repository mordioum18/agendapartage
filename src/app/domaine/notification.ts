import {Evenement} from './evenement';

export class Notification {
    public id: number;
    public dateEnvoi: number;
    public texte: string;
    public evenement: Evenement; }