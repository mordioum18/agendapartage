import {Agenda} from './agenda';
import {User} from './user';

export class UserAgendaPk {
    public user: User ;
    public agenda: Agenda ;
}