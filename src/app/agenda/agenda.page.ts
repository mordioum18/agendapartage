import { Component, OnInit } from '@angular/core';
import {AgendaService} from '../service/agenda.service';
import {Agenda} from '../domaine/agenda';
import {UserService} from '../service/user.service';
import { ModalController } from '@ionic/angular';
import {ModalPage} from '../add-evenement/modal/modal.page';
import {FormAgendaPage} from '../form-agenda/form-agenda.page';


@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.page.html',
  styleUrls: ['./agenda.page.scss'],
})
export class AgendaPage implements OnInit {
  agendas: Agenda[] ;
  numberUser: number[] = [] ;
  constructor(private agendaService: AgendaService, private userService: UserService, private modalController: ModalController ) { }

    ngOnInit() {
        this.userService.getAgenda(1).subscribe(
            data => {
                this.agendas = data ;
                console.log(this.agendas) ;
                for(const agenda of this.agendas) {
                    this.agendaService.getUsers(agenda.id).subscribe(
                        data2 => {
                            this.numberUser[agenda.id] = data2.length ;
                            console.log(this.numberUser) ;
                        }
                    );
                }
            }
        );

    }
    async openModalForm() {
        const modal = await this.modalController.create({
            component: FormAgendaPage,
            componentProps: { value: 123 },
        });
        modal.present();
        modal.onDidDismiss().then(
            data => (
                console.log(data.data.data3),
                this.agendas.push(data.data.data3)
            )
        );
    }

}
