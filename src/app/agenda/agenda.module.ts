import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AgendaPage } from './agenda.page';
import {AgendaService} from '../service/agenda.service';
import {UserService} from '../service/user.service';
import {FormAgendaPage} from '../form-agenda/form-agenda.page';
import {ShowEventPage} from '../add-evenement/show-event/show-event.page';
import {ModalPage} from '../add-evenement/modal/modal.page';
import {EvenementUsersPage} from '../add-evenement/evenement-users/evenement-users.page';

const routes: Routes = [
  {
    path: '',
    component: AgendaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AgendaPage, FormAgendaPage],
    providers: [AgendaService, UserService],
    entryComponents: [FormAgendaPage]
})
export class AgendaPageModule {}
