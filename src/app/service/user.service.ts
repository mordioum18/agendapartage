import {API_URLS} from '../../config/api.url.config';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {User} from '../domaine/user';
import {Observable} from 'rxjs';

@Injectable()
export class UserService {
    baseURL = API_URLS.USER_URL;


    constructor(private http: HttpClient) {
    }

    create(user: User): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        httpHeaders.set('Accept', 'application/json');
        return this.http.post(this.baseURL, user, {headers: httpHeaders})
    }
    get(id: number): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        httpHeaders.set('Accept', 'application/json');
        return this.http.get(this.baseURL + '/' + id, {headers: httpHeaders}) ;
    }
    getAgenda(id: number): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        httpHeaders.set('Accept', 'application/json');
        return this.http.get(this.baseURL + '/' + id + '/agendas', {headers: httpHeaders}) ;
    }
    createUserAgenda(idUser: number, idAgenda: number): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        httpHeaders.set('Accept', 'application/json');
        return this.http.get(this.baseURL + '/agenda/user_agenda/' + idUser + '/' + idAgenda, {headers: httpHeaders}) ;
    }

}
