import {API_URLS} from '../../config/api.url.config';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Notification} from '../domaine/notification';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
@Injectable()
export class  NotificationService {
    baseURL = API_URLS.NOTIFICATION_URL ;


    constructor(private http: HttpClient) {
    }
    create(notification: Notification,  tabIdNumber: number[]): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        httpHeaders.set('Accept', 'application/json');
        return this.http.post(this.baseURL , notification, {headers: httpHeaders}) ;

    }
    get(id: number): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        httpHeaders.set('Accept', 'application/json');
        return this.http.get(this.baseURL + '/' + id, {headers: httpHeaders} ) ;
    }
}