import {API_URLS} from '../../config/api.url.config';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Evenement} from '../domaine/evenement';
import {Agenda} from '../domaine/agenda';
import {Observable} from 'rxjs';

@Injectable()
export class AgendaService {
    baseURL = API_URLS.AGENDA_URL


    constructor(private http: HttpClient) {
    }
    create(agenda: Agenda,  tabIdNumber: number[]): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        httpHeaders.set('Accept', 'application/json');
        return this.http.post(this.baseURL , agenda, {headers: httpHeaders}) ;

    }
    get(id: number): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        httpHeaders.set('Accept', 'application/json');
        return this.http.get(this.baseURL + '/' + id, {headers: httpHeaders} ) ;

    }
    getUsers(id: number): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        httpHeaders.set('Accept', 'application/json');
        return this.http.get(this.baseURL + '/' + id + '/users', {headers: httpHeaders} ) ;

    }


}