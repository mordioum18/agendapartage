import {API_URLS} from '../../config/api.url.config';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {UserAgenda} from '../domaine/userAgenda';

@Injectable()
export class UserUserAgendaErvice {


    constructor(private http: HttpClient) {
    }
    baseURL = API_URLS.AGENDA_URL ;
    create(userUserAgenda: UserAgenda): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        httpHeaders.set('Accept', 'application/json');
        return this.http.post(this.baseURL, userUserAgenda, {headers: httpHeaders}) ;

    }
    get(id: number): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        httpHeaders.set('Accept', 'application/json');
        return this.http.get(this.baseURL + '/' + id, {headers: httpHeaders} ) ;
    }
}