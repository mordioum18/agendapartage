import {API_URLS} from '../../config/api.url.config';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Calendrier} from '../domaine/calendrier';
import {Observable} from 'rxjs';

@Injectable()
export class CalendrierService {
    baseURL = API_URLS.CALENDRIER_URL ;
    constructor(private http: HttpClient) {
    }
    create(calendrier: Calendrier,  tabIdNumber: number[]): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        httpHeaders.set('Accept', 'application/json');
        return this.http.post(this.baseURL , calendrier, {headers: httpHeaders}) ;

    }
    get(id: number): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        httpHeaders.set('Accept', 'application/json');
        return this.http.get(this.baseURL + '/' + id, {headers: httpHeaders} ) ;
    }
    getEventsBycalendrierAndMonthBetween(id: number, debut: Date, fin: Date) {
        const httpHeaders = new HttpHeaders();
        const httParam = new HttpParams() ;
        httpHeaders.set('Content-Type', 'application/json');
        httpHeaders.set('Accept', 'application/json');
        httParam.append('debut', debut.getTime().toString()) ;
        httParam.append('fin', fin.getTime().toString()) ;
        return this.http.post(this.baseURL + '/' + id + '/evenementsBymonth', event, {headers: httpHeaders, params: httParam}) ;
    }

}