import {API_URLS} from '../../config/api.url.config';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Evenement} from '../domaine/evenement';

@Injectable()
export class EvenementService {
    baseURL = API_URLS.EVENT_URL ;
    constructor(private http: HttpClient) {
    }
    create(event: Evenement,  tabIdNumber: number[]) {
        const httpHeaders = new HttpHeaders();
        const tabString = this.tabNumberToString(tabIdNumber) ;
        const httParam = new HttpParams()
            .append('usersIdString', tabString);
        httpHeaders.set('Content-Type', 'application/json');
        httpHeaders.set('Accept', 'application/json');

        return this.http.post(this.baseURL, event, {headers: httpHeaders, params: {usersIdString: tabString}}) ;

    }
    private tabNumberToString (tabNumber: number[]): string {
        const resultTab: string[] = [] ;
        for ( const id of tabNumber){
            resultTab.push(id.toString()) ;
        }
        return resultTab.join(',') ;

    }
    delete(id: number) {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        httpHeaders.set('Accept', 'application/json');
        return this.http.post(this.baseURL + '/' + id, {headers: httpHeaders, responseType: 'text'}) ; ;
    }

}