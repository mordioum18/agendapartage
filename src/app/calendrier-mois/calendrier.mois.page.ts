import {Component, OnInit, ViewChild} from '@angular/core';
import {IonSlides, NavController} from '@ionic/angular';
import {Router} from '@angular/router';
import {CalendrierPage} from '../calendrier/calendrier.page';


@Component({
    selector: 'app-calendrier-mois',
    templateUrl: 'calendrier.mois.page.html',
    styleUrls: ['calendrier.mois.page.scss'],
})
export class CalendrierMoisPage implements OnInit{
    @ViewChild('slider') private slider: IonSlides;
    @ViewChild('currentMonth') private currentMonth: CalendrierPage;
    @ViewChild('nextMonth') private nextMonth: CalendrierPage;
    @ViewChild('prevMonth') private prevMonth: CalendrierPage;
    lastMaxDate: Date;
    currentDate: Date;
    lastMinDate: Date;
    events: any[];
    slideOpts = {
        spaceBetween: 10,
    };
    options1: any;
    options2: any;
    options3: any;

    constructor(private router: Router, private navCtrl: NavController) {
    }
    createOption(){
        this.options2 = {
            defaultDate: this.currentDate,
            height: 'parent',
            locale: 'fr',
            titleFormat: { // will produce something like "Tuesday, September 18, 2018"
                month: 'long',
                year: 'numeric',
                day: 'numeric',
                weekday: 'long'
            },
            buttonText: {
                month: 'Mois',
                week: 'Semaine',
                day: 'Jour',
                list:  'List'
            },
            header: {
                left: '',
                right: ''
            },
            dateClick: (e) => {
                this.navigateToDay(e.dateStr) ;
            }
        };
        this.options1 = JSON.parse(JSON.stringify(this.options2));
        this.options1.defaultDate = this.lastMinDate;
        this.options3 = JSON.parse(JSON.stringify(this.options2));
        this.options3.defaultDate = this.lastMaxDate;
    }

    ngOnInit() {
        this.events = [
            {
                id : 1,
                title: 'All Day Event',
                start: '2019-01-01',
            },
            {
                id : 2,
                title: 'Long Event',
                start: '2019-01-07',
                end: '2019-01-10'
            }
        ];
        this.currentDate = new Date();
        this.lastMinDate = this.minusMois(this.currentDate);
        this.lastMaxDate = this.addMois(this.currentDate);
        this.createOption();
    }
    onSlideNextEnd(){
        this.currentMonth.fc.getCalendar().next();
        this.nextMonth.fc.getCalendar().next();
        this.prevMonth.fc.getCalendar().next();
        this.currentDate = new Date(this.currentMonth.fc.getCalendar().getDate()) ;
        this.slider.slideTo(1, 0, false);
        this.router.navigate(['/calendrier-semaine']);
    }
    onSlidePrevEnd(){
        this.currentMonth.fc.getCalendar().prev();
        this.nextMonth.fc.getCalendar().prev();
        this.prevMonth.fc.getCalendar().prev();
        this.currentDate = new Date(this.currentMonth.fc.getCalendar().getDate()) ;
        this.slider.slideTo(1, 0, false);
    }
    onChange(event){
    }
    addMois(date: Date): Date{
        if (date.getMonth() === 11) {
            return new Date(date.getFullYear() + 1, 0, 1);
        } else {
            return new Date(date.getFullYear(), date.getMonth() + 1, 1);
        }
    }
    minusMois(date: Date): Date{
        if (date.getMonth() === 0) {
            return new Date(date.getFullYear() - 1, 11, 1);
        } else {
            return new Date(date.getFullYear(), date.getMonth() - 1, 1);
        }
    }
    ngAfterViewInit() {
        this.slider.slideTo(1, 0, false);
    }
    navigateToDay(date: string){
        this.router.navigate(['calendrier-jour/gotodate'], { queryParams: { date: date } }) ;
    }
    navigateToAdd_evenement() {
        this.navCtrl.navigateForward('/add-evenement')
    }
}
