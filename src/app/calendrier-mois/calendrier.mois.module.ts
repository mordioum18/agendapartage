import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { CalendrierMoisPage } from './calendrier.mois.page';
import {FullCalendarModule} from 'primeng/fullcalendar';
import {SharedModuleModule} from '../shared-module/shared-module.module';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: CalendrierMoisPage
      }
    ]),
      FullCalendarModule,
      SharedModuleModule,

  ],
  declarations: [
      CalendrierMoisPage,
  ],
})
export class CalendrierMoisPageModule {}
