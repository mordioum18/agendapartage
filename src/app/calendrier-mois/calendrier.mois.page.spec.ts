import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendrierMoisPage } from './calendrier.mois.page';

describe('CalendrierMoisPage', () => {
  let component: CalendrierMoisPage;
  let fixture: ComponentFixture<CalendrierMoisPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendrierMoisPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendrierMoisPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
