import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
    {
        path: 'calendrier-mois',
        loadChildren: './calendrier-mois/calendrier.mois.module#CalendrierMoisPageModule'
    },
  { path: 'calendrier-semaine', loadChildren: './calendrier-semaine/calendrier-semaine.module#CalendrierSemainePageModule' },
  { path: 'calendrier-jour', loadChildren: './calendrier-jour/calendrier-jour.module#CalendrierJourPageModule' },
  { path: 'add-evenement', loadChildren: './add-evenement/add-evenement.module#AddEvenementPageModule' },  { path: 'show-event', loadChildren: './add-evenement/show-event/show-event.module#ShowEventPageModule' },
  { path: 'evenement-users', loadChildren: './add-evenement/evenement-users/evenement-users.module#EvenementUsersPageModule' },
  { path: 'agenda', loadChildren: './agenda/agenda.module#AgendaPageModule' },
  { path: 'form-agenda', loadChildren: './form-agenda/form-agenda.module#FormAgendaPageModule' },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
