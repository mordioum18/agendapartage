import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-evenement-users',
  templateUrl: './evenement-users.page.html',
  styleUrls: ['./evenement-users.page.scss'],
})
export class EvenementUsersPage implements OnInit {

    constructor(public modalController: ModalController, public navParams: NavParams) { }

    ngOnInit() {
    }
    public form = [
        { val: 'user1', isChecked: false },
        { val: 'user2', isChecked: false },
        { val: 'user3', isChecked: false }
    ];
    dismiss() {
        const data = [];
        for (let i = 0 ; i <  this.form.length ; i++) {
            if (this.form[i].isChecked) {
                data[i] = this.form[i];
            }
        }
        this.modalController.dismiss({
            data
        });
    }

}
