import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvenementUsersPage } from './evenement-users.page';

describe('EvenementUsersPage', () => {
  let component: EvenementUsersPage;
  let fixture: ComponentFixture<EvenementUsersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvenementUsersPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvenementUsersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
