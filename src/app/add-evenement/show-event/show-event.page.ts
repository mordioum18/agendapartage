import {Component, Input, OnInit} from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-show-event',
  templateUrl: './show-event.page.html',
  styleUrls: ['./show-event.page.scss'],
})
export class ShowEventPage implements OnInit {

  @Input() value: number;
  constructor(public modalController: ModalController, public navParams: NavParams) { }

  ngOnInit() {
  }
  dismiss(){
    this.modalController.dismiss();
  }

}
