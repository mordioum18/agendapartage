import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import {CalendrierPage} from '../calendrier/calendrier.page';
import {CalendrierMoisPage} from '../calendrier-mois/calendrier.mois.page';
import {ShowEventPage} from './show-event/show-event.page';
import {ModalPage} from './modal/modal.page';
import {EvenementUsersPage} from './evenement-users/evenement-users.page';

@Component({
    selector: 'app-add-evenement',
    templateUrl: './add-evenement.page.html',
    styleUrls: ['./add-evenement.page.scss'],
})
export class AddEvenementPage implements OnInit {
   cals:  any;
   users: any;
    constructor(public modalController: ModalController) { }

    ngOnInit() {
    }

    public form = [
        { val: 'calendrier1', isChecked: false },
        { val: 'Calendrier2', isChecked: false },
        { val: 'Calendri3', isChecked: false }
    ];

    async presentModal() {
        const modal = await this.modalController.create({
            component: ModalPage,
            componentProps: { value: 123 },
        });
        modal.present();
        modal.onDidDismiss().then(
            data => (
                    console.log(data.data.data),
                        this.cals = data.data.data
            )
        );
    }

    async addUsers() {
        const modal = await this.modalController.create({
            component: EvenementUsersPage,
            componentProps: { value: 123 },
        });
        modal.present();
        modal.onDidDismiss().then(
            data => (
                console.log(data.data.data),
                    this.users = data.data.data
            )
        );
    }
}
