import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {

  constructor(public modalController: ModalController, public navParams: NavParams) { }

  ngOnInit() {
  }
    public form = [
        { val: 'calendrier1', isChecked: true },
        { val: 'Calendrier2', isChecked: false },
        { val: 'Calendri3', isChecked: false }
    ];
  dismiss() {
    const data = [];
      for (let i = 0 ; i <  this.form.length ; i++) {
        if (this.form[i].isChecked) {
          data[i] = this.form[i];
        }
      }
        this.modalController.dismiss({
            data
        });
    }

}
