import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEvenementPage } from './add-evenement.page';

describe('AddEvenementPage', () => {
  let component: AddEvenementPage;
  let fixture: ComponentFixture<AddEvenementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEvenementPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEvenementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
