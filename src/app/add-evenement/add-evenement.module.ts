import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddEvenementPage } from './add-evenement.page';
import {ShowEventPage} from './show-event/show-event.page';
import {ShowEventPageModule} from './show-event/show-event.module';
import {ModalPage} from './modal/modal.page';
import {EvenementUsersPage} from './evenement-users/evenement-users.page';

const routes: Routes = [
  {
    path: '',
    component: AddEvenementPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [AddEvenementPage,ShowEventPage,ModalPage,EvenementUsersPage],
    entryComponents: [ShowEventPage,ModalPage,EvenementUsersPage]
})
export class AddEvenementPageModule {}
