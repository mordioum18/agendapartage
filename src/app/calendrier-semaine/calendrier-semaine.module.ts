import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CalendrierSemainePage } from './calendrier-semaine.page';
import {FullCalendarModule} from 'primeng/fullcalendar';
import {CalendrierPage} from '../calendrier/calendrier.page';
import {SharedModuleModule} from '../shared-module/shared-module.module';
import {EvenementService} from '../service/evenement.service';

const routes: Routes = [
  {
    path: '',
    component: CalendrierSemainePage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        FullCalendarModule,
        SharedModuleModule,
    ],
    providers: [EvenementService],
    declarations: [CalendrierSemainePage],
    entryComponents: [CalendrierSemainePage],
})
export class CalendrierSemainePageModule {}
