import { Component, OnInit, ViewChild } from '@angular/core';
import {IonSlides, NavController} from '@ionic/angular';
import {ActivatedRoute, Router} from '@angular/router';
import {CalendrierPage} from '../calendrier/calendrier.page';
import {EvenementService} from '../service/evenement.service';
import {Evenement} from '../domaine/evenement';

@Component({
  selector: 'app-calendrier-semaine',
  templateUrl: './calendrier-semaine.page.html',
  styleUrls: ['./calendrier-semaine.page.scss'],
})
export class CalendrierSemainePage implements OnInit {
    @ViewChild('slider') private slider: IonSlides;
    @ViewChild('currentWeek') private currentWeek: CalendrierPage;
    @ViewChild('nextWeek') private nextWeek: CalendrierPage;
    @ViewChild('prevWeek') private prevWeek: CalendrierPage;
    tabWeek: any[] = [];
    currentDate: Date ;
    lastMaxDate: Date;
    lastMinDate: Date;
    events: any[];
    firstSlide = true;
    slideOpts = {
        spaceBetween: 10,
        //loop: true
    };
    options1: any  ;
    options2: any ;
    options3: any ;
    constructor(private router: Router, private eventService: EvenementService, private navCtrl: NavController) {
    }
    ngOnInit() {
        this.events = [
            {
                'title': 'All Day Event',
                'start': '2019-01-01',
            },
            {
                'title': 'Long Event',
                'start': '2019-01-07',
                'end': '2019-01-10'
            },
            {
                'title': 'mor dioum',
                'start': '2019-01-07',
                'end': '2019-01-10',
                'color': '#378006'
            },
            {
                'title': 'Repeating Event',
                'start': '2019-01-09T16:00:00'
            },
            {
                'title': 'Repeating Event',
                'start': '2019-01-16T16:00:00',
                'color': '#378006'
            },
            {
                'title': 'Conference',
                'start': '2019-01-11',
                'end': '2019-01-13'
            }
        ];
        this.currentDate = new Date();
        this.createOption() ;
        this.lastMaxDate = this.addWeek(this.currentDate);
        this.lastMinDate = this.minusWeek(this.currentDate);
        const event = new Evenement() ;
        event.dateCreation = 1548201600000 ;
        event.dateDebut = 1548201600000 ;
        event.dateFin = 1548201600000 ;
        event.description = 'jjdjfjfjd' ;
        event.estFige = true ;
        event.nom =  'fjghngkfn' ;
        this.eventService.create(event, [1, 4] ).subscribe(
            data => {
                console.log(data) ;
            },
            error1 => {
                console.log(error1);
            }
        ) ;

    }
    createOption(){
        this.options2 = {
            defaultDate: this.currentDate,
            defaultView: 'agendaWeek',
            firstDay: 1,
            height: 'parent',
            locale: 'fr',
            buttonText: {
                month: 'Mois',
                week: 'Semaine',
                day: 'Jour',
                list:  'List'
            },
            header: {
                left: '',
                right: ''
            },
            dateClick: (e) => {
                console.log('hhhhhhhhhhhhhhhhhh');
                ;
            }
        };
        this.options1 = JSON.parse(JSON.stringify(this.options2)) ;
        this.options1.defaultDate = this.lastMinDate ;
        this.options3 = JSON.parse(JSON.stringify(this.options2)) ;
        this.options3.defaultDate = this.lastMaxDate ;
    }
    onSlideNextEnd(){
        this.currentWeek.fc.getCalendar().next();
        this.nextWeek.fc.getCalendar().next();
        this.prevWeek.fc.getCalendar().next();
        this.currentDate = this.currentWeek.fc.getCalendar().getDate() ;

        this.slider.slideTo(1, 0, false) ;

    }
    onSlidePrevEnd(){
        this.currentWeek.fc.getCalendar().prev() ;
        this.nextWeek.fc.getCalendar().prev() ;
        this.prevWeek.fc.getCalendar().prev() ;
        this.currentDate = this.currentWeek.fc.getCalendar().getDate() ;
        this.slider.slideTo(1, 0, false) ;

    }
    onChange(event){
    }
    addWeek(date: Date): Date{
        const week3 = new Date(date)
        week3.setDate(date.getDate() + 7)
        return week3;
    }
    minusWeek(date: Date){
        const  week1 = new Date(date) ;
        week1.setDate(date.getDate() - 7);
        return week1 ;
    }
    ngAfterViewInit() {
        this.slider.slideTo(1, 0, false);
    }

    private getMonday(date) {
        date = new Date(date);
        const ms = -date.getDay() + 1;
        date.setDate(date.getDate() + ms);
        return date;
    }
    navigateToDay(date: string){
        this.events = this.events.concat([
            {
                'title': 'All Day Event',
                'start': '2019-01-20',
            }]) ;
        this.router.navigate(['calendrier-jour/gotodate'], { queryParams: { date: date } }) ;

    }
    updateOptions() {
        this.options2.defaultDate = this.currentDate ;
        this.options2 = JSON.parse(JSON.stringify(this.options2)) ;
        this.lastMaxDate = this.addWeek(this.currentDate) ;
        this.lastMinDate = this.minusWeek(this.currentDate) ;
        this.options1 = JSON.parse(JSON.stringify(this.options2)) ;
        this.options1.defaultDate = this.lastMinDate ;
        this.options3 = JSON.parse(JSON.stringify(this.options2)) ;
        this.options3.defaultDate = this.lastMaxDate ;
    }
    navigateToAdd_evenement() {
        this.navCtrl.navigateForward('/add-evenement')
    }
}
