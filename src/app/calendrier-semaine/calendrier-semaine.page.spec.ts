import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendrierSemainePage } from './calendrier-semaine.page';

describe('CalendrierSemainePage', () => {
  let component: CalendrierSemainePage;
  let fixture: ComponentFixture<CalendrierSemainePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendrierSemainePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendrierSemainePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
