import {Component, Input, OnChanges, ViewChild, ViewChildren} from '@angular/core';
import {FullCalendar} from 'primeng/fullcalendar';


@Component({
  selector: 'app-calendrier',
  templateUrl: 'calendrier.page.html',
  styleUrls: ['calendrier.page.scss'],
})
export class CalendrierPage implements OnChanges{
    @ViewChild('fc') public fc: FullCalendar;
    @Input() events: any[];
    @Input() options: any;
    ngOnChanges(){
        if (this.fc !== undefined && this.fc.getCalendar() !== undefined){
            //this.fc.getCalendar().gotoDate(this.options.defaultDate);
        }
    }

}
