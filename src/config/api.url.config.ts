const adresse = 'http://localhost:8080/';

export const API_URLS = {
  EVENT_URL: adresse + 'evenements',
  CALENDRIER_URL: adresse + 'calendriers',
  USER_URL: adresse + 'users',
  AGENDA_URL: adresse + 'agendas',
  NOTIFICATION_URL: 'notifications'

}
